<?php 
    require_once("vendor/autoload.php");
    $latte = new Latte\Engine;
    $latte->setTempDirectory('temp');

    $bmi = "";
    $age = -1;

    function ageCategory($age) {
        switch ($age) {
            case 18:
                return 1;
                break;
            case 19:
                return 2;
                break;
            case 20:
                return 3;
                break;
            case 21:
                return 4;
                break;
            case 22:
                return 5;
                break;
            case 23:
                return 6;
                break;
            case 24:
                return 7;
                break;
            default:
                return "";
                break;
        }
    }

    if (isset($_GET['weight']) && isset($_GET['height']) && isset($_GET['age'])) {
        $weight = (int) $_GET['weight'];
        $height = (int) $_GET['height'];
        $age = (int) $_GET['age'];
        echo $weight / ($height * $height);
        $bmi = $weight / ($height * $height);
    }

    $params = [
        'bmi' => $bmi,
    ];
    
    // kresli na výstup
    $latte->render('template/bmiTemplate.latte', $params);
?>