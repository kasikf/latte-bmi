<?php

use Latte\Runtime as LR;

/** source: template/bmiTemplate.latte */
final class Template3a4a83de1b extends Latte\Runtime\Template
{
	public const Source = 'template/bmiTemplate.latte';


	public function main(array $ʟ_args): void
	{
		extract($ʟ_args);
		unset($ʟ_args);

		echo '<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>BMI kalkulačka</title>
</head>
<body>
    <h1>BMI kalkulačka</h1>

    <form action="bmi.php" method="get">
        <div class="form-group">
            <label for="weight">Váha</label>
            <input type="number" class="form-control" id="weight" name="weight">
        </div>

        <div class="form-group">
            <label for="height">Výška</label>
            <input type="number" class="form-control" id="height" name="height">
        </div>

        <div class="form-group">
            <label for="age">Věk</label>
            <input type="number" class="form-control" id="age" name="age">
        </div>

        <input type="submit" value="Vypočti">
    </form>
    <p>Tvoje bmi: ';
		echo LR\Filters::escapeHtmlText($bmi) /* line 29 */;
		echo '</p>
</body>
</html>';
	}
}
